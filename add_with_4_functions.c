//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>

int input()
{
	int a;
	printf("Enter the number :");
	scanf("%d",&a);
	return a;
}
int sum (int a , int b)	
{
	int sum;
	sum = a+b;
	return sum;

}
int output(int a, int b, int c)
{
	printf("The sum of %d + %d is %d",a,b,c);
	
}
int main()
{
	int x,y,z,n;
	x= input();
	y= input();
	z= sum(x,y);
	n= output(x,y,z);
	return 0;
}

