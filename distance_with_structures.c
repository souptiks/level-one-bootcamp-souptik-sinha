#include<stdio.h>
#include<math.h>
struct point
{
	float x,y;
};
typedef struct point Point;

Point input()
{
	Point p;
	printf("Enter the value of x ");
	scanf("%f",&p.x);
printf("Enter the value of y ");
	scanf("%f",&p.y);
	return p;

} 
float compute (Point p1, Point p2)
{
	float dis;
	dis =	sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2));
	return dis;
}
void output(Point p1, Point p2,float dis)
{
	
	printf("The distance between the 2 points (%f,%f) and (%f,%f) is : %f",p1.x,p1.y,p2.x,p2.y,dis);

}
int main()
{
	Point p1,p2;
	float d;
	printf("Enter point 1");
	p1 = input ();
   	printf("\nEnter point 2");
	p2 = input ();
	d = compute(p1,p2);
	output(p1,p2,d);
	return 0;
}

